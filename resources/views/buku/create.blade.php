@extends('layout/main');

@section('title', 'Form ')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-2">Form Data Perpustakaan</h1>
                    <form method="post" action="/buku/">
                    @csrf
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukkan Nama Buku" name="nama">
                            @error('nama')
                            <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="text" class="form-control @error('kode') is-invalid @enderror" id="kode" placeholder="Masukkan Kode Buku" name="kode">
                            @error('kode')
                            <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="kategori">Kategori</label>
                            <input type="kategori" class="form-control @error('kategori') is-invalid @enderror" id="kategori" placeholder="Masukkan kategori  Buku" name="kategori">
                            <option value="{{ $id }}">$bk->kategoris->nama</option>
                            @error('kategori')
                            <div class="invalid-feedback">{{$message}}</div>
                            @enderror
                            
                                
                                    
                                
                            
                                
                            
        </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah Data </button>
                        
                    </form>

            </div>
           
        </div>
    </div>
@endsection

    