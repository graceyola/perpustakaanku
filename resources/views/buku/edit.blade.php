@extends('layout/main');

@section('title', 'Form ')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-2">Form ubah Data Perpustakaan</h1>
                @foreach($bukus as $bk)
                    <form method="post" action="/buku/index">
                    {{csrf_field()}}
                    
                    
                    @csrf
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" required="required" class="form-control" id="nama" value="{{$bk->nama}}" placeholder="Masukkan Nama Buku" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="text" required="required" class="form-control" id="kode" value="{{$bk->kode}}" placeholder="Masukkan Kode Buku" name="kode">
                        </div>
                        <div class="form-group">
                            <label for="kategori">kategori</label>
                            <input type="text" required="required" class="form-control" id="kategori" value="{{ $bk->kategoris->nama }}" placeholder="Masukkan kategori Buku" name="kategori">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Ubah Data </button>
                        
                    </form>
                @endforeach

            </div>
           
        </div>
    </div>
@endsection

    