@extends('layout/main');

@section('title', 'CRUD Perpustakaan')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-2">Daftar Buku </h1>

                

                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">nama</th>
                            <th scope="col">Kode</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">aksi</th>

                        </tr>
                    </thead>
                    <tbody>
                     @foreach($buku as $bk )
                         
                    <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $bk->nama }} </td>
                            <td>{{ $bk->kode }}</td>
                            <td>{{ $bk->kategoris->nama }}</td>
                             <td>
                                <a href="/buku/edit/{{$bk->id}}" class="btn btn-primary">Edit</a>
                                

                                <a href="/delete/{{ $bk->id }}" class="btn btn-danger" >Delete</a>
                                
                        </tr>
                    @endforeach 
 
                    </tbody>
                </table>

                <a href="/buku/create" class="btn btn-primary my-2"> Tambah Data Buku </a>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                @endif
                
                {{ $bukus->links() }}


            </div>
        </div>
    </div>
@endsection

    