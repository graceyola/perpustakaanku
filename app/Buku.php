<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table ='bukus';
    protected $fillable =['nama', 'kode', 'kategori'];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
}
