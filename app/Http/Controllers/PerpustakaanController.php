<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Buku;
use App\Kategori;

class PerpustakaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bukus =\App\Buku::all();
        $bukus = DB::table('bukus')->paginate(5);

        
        

        return view('buku.index', ['buku'=>$bukus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('buku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'kode' => 'required|size:9'

        ]);



        $bk = new Buku;
        $bk->nama = $request->nama;
        $bk->kode = $request->kode;
        $bk->kategori = $request->kategori;

        $dft->save();

        return redirect('/buku')->with('status','Data buku berhasil ditambahkan'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bukus = DB::table('bukus')->where('id',$id)->get();
        return view('buku.edit', ['bukus'=> $bukus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('bukus')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'kode' => $request->kode,
            'kategori' => $request->kategori
            ]);

          

           
        
            return redirect('/perpustakaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::table('bukus')->where('id', $id)->delete();

        return redirect('/buku');
    }
}
