<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');


Route::get('/buku', 'PerpustakaanController@index');
Route::get('/buku/create', 'PerpustakaanController@create');
Route::post('/buku', 'PerpustakaanController@store');
Route::get('/buku/edit/{id}', 'PerpustakaanController@edit');
Route::post('/bukun/update', 'PerpustakaanController@update'); 
Route::get('/delete/{id}', 'PerpustakaanController@delete'); 

